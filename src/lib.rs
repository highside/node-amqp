//! Utilities for producing and consuming AMQP messages.
//!
//! # Overview
//! This currently provides two abstractions:
//! - [AmqpSender], which sends messages over AMQP
//! - [AmqpReceiver], which receives messages from topic exchanges on an AMQP broker
//!
//! [AmqpSender] is a worker that consumes from a queue of messages, sending them via its channel as quickly as it
//! can. It can receive messages from many threads at once.
//!
//! [AmqpReceiver] is a worker that receives messages sent to topic exchanges that match any of an arbitrary number of
//! patterns. It provides a [crossbeam::Receiver] that receives [IncomingMessage]s.
//!
//! # Sending messages
//! Create an [AmqpSender] from an AMQP channel, then send types that implement [serde::Serialize].
//!
//! There are two ways to use an [AmqpSender]. You can use it to send things directly:
//! ```rust,ignore
//! let sender = AmqpSender::new(some_amqp_channel);
//! sender.send("exchange", "routing.key", message);
//! ```
//!
//! Or you can use its `outgoing` channel to send things from multiple threads:
//! ```rust,ignore
//! #[derive(Serialize)]
//! struct Example {
//!   i: i32,
//!   j: i32,
//! }
//!
//! let sender = AmqpSender::new(some_amqp_channel);
//! let outgoing = sender.outgoing.clone();
//!
//! std::thread::spawn(move || {
//!   let another_example = Example { i: 30, j: 40 };
//!   outgoing.send(
//!     another_example.to_message(
//!         "exchange",
//!         "routing.key"
//!     ).expect("Couldn't serialize another_example"));
//! });
//! ```
//!
//! # Receiving messages
//! Configure an [AmqpReceiver], then start it with an [amqp::channel::Channel].
//!
//! ## Creation and configuration
//! You can't add or remove bindings on exchanges once you start receiving AMQP messages, so you have to do all of that
//! upfront:
//! ```rust
//! extern crate node_amqp;
//! use node_amqp::AmqpReceiver;
//!
//! let mut receiver = AmqpReceiver::new();
//!
//! // Receive messages sent to the `amq.topic` exchange matching pattern `routing.key`.
//! receiver.listen(
//!   "amq.topic".to_string(),
//!   "routing.key".to_string()
//! );
//!
//! // Also receive messages sent to `my.exchange` with any routing key:
//! receiver.listen(
//!   "my.exchange".to_string(),
//!   "*".to_string()
//! );
//! ```
//!
//! ## Usage
//! When you're done configuring and want to start receiving messages, you give `receiver` a [amqp::Channel]. This
//! consumes it and returns a [crossbeam::Receiver].
//!
//! The [crossbeam::Receiver] receives [IncomingMessage]s, which have some stuff in them and almost definitely have
//! their `body` available as some bytes:
//! ```rust,ignore
//! # #[macro_use] extern crate log;
//! let rx: crossbeam::channel::Receiver<IncomingMessage> = {
//!   let mut receiver = AmqpReceiver::new();
//!
//!   receiver.listen(
//!     "amq.topic".to_string(),
//!     "routing.key".to_string()
//!   );
//!
//!   receiver
//!        .start_receiving(recv_channel)
//!        .expect("Failed to start receiver")
//! };
//!
//! // do message passing things
//! while let Some(msg) = rx.recv() {
//!   let msg = format!(
//!       "{:?}: {:#08X} ({}) bytes",
//!       msg,
//!       msg.body.len(),
//!       msg.body.len()
//!   );
//!
//!   info!("{}", msg);
//! }
//! ```
//!

// These can be replaced with scoped lints (e.g. #![allow(clippy::unit_arg)]) eventually
#![cfg_attr(feature = "cargo-clippy", deny(all))]
#![cfg_attr(feature = "cargo-clippy", allow(unit_arg))]

extern crate amqp;
extern crate crossbeam;
#[macro_use]
extern crate log;
extern crate parking_lot;
extern crate serde_json;

mod amqp_receiver;
mod amqp_sender;
mod node_amqp_error;

pub use crate::amqp_receiver::*;
pub use crate::amqp_sender::*;
pub use crate::node_amqp_error::*;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
