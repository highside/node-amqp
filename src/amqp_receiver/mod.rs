use amqp::Basic;
use crossbeam::channel;
use node_amqp_error::*;
use std::thread;

/// An AMQP message received by an [AmqpReceiver].
///
/// # Overview
/// An `IncomingMessage` is sent over the channel returned by [AmqpReceiver::start_receiving] whenever an [AmqpReceiver]
/// receives an AMQP message.
///
/// I have no idea what this should look like, so the only thing unlikely to change is `body`.
#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub struct IncomingMessage {
    /// The AMQP message's body.
    pub body: Vec<u8>,

    /// If provided, the AMQP message's `reply-to`.
    pub reply_to: Option<String>,

    /// If provided, the AMQP message's `correlation-id`.
    pub correlation_id: Option<String>,

    /// If provided, the AMQP message's MIME `content-type`.
    pub content_type: Option<String>,

    /// If provided, the AMQP message's MIME `content-encoding`.
    pub content_encoding: Option<String>,

    /// The routing key name specified when the message was published.
    pub routing_key: String,

    /// The exchange the message was originally published to.
    pub exchange: String,
}

/// `AmqpReceiverConsumer`s are [amqp::Consumer]s built by an [AmqpReceiver].
#[derive(Debug, Clone)]
struct AmqpReceiverConsumer(channel::Sender<IncomingMessage>);

/// Creates an AMQP queue, sets up various bindings to it, and returns messages sent to it on an unbounded [channel].
///
/// # Examples
/// ```rust,ignore
/// let rx: crossbeam::channel::Receiver<IncomingMessage> = {
///   let mut receiver = AmqpReceiver::new();
///
///   receiver.listen(
///     "amq.topic".to_string(),
///     "routing.key".to_string()
///   );
///
///   receiver
///        .start_receiving(recv_channel)
///        .expect("Failed to start receiver")
/// };
/// ```
///
/// # TODOs
/// - Error handling
///
/// # Panics
/// Likely maliciously and at random, given that it's largely untested.
#[derive(Debug, Eq, PartialEq, Default, Clone)]
pub struct AmqpReceiver {
    to_bind: Vec<(String, String)>, // TODO actual type
}

impl AmqpReceiver {
    /// Create a new [AmqpReceiver].
    pub fn new() -> Self {
        AmqpReceiver::default()
    }

    /// Configures `self` to bind `routing_key` on `exchange` to its queue when `start_receiving` is called.
    ///
    /// # Examples
    /// ```rust,ignore
    /// receiver.listen("amq.topic".to_string(), "routing.key".to_string());
    /// ```
    pub fn listen<S: Into<String>>(&mut self, exchange: S, routing_key: S) {
        // TODO handle duplicates here
        self.to_bind.push((exchange.into(), routing_key.into()));
    }

    /// Consumes the [AmqpReceiver], spawning a thread that uses an [amqp::Channel] to receive messages as previously
    /// configured by calling `listen`.
    ///
    /// # Returns
    /// The receiving half of a [channel]. Once consuming has begun—which happens at some undefined point in the future
    /// after this call—incoming messages will be received on that channel.
    pub fn start_receiving(
        self,
        mut channel: amqp::Channel,
    ) -> NodeAmqpResult<channel::Receiver<IncomingMessage>> {
        // If we weren't actually configured to listen to anything, it made no sense to call this. We could statically
        // prevent this by having the first `listen` call consume this and return some `NonEmptyBuilder`, perhaps? Or
        // just require these to be constructed with an initial exchange and routing key
        if self.to_bind.is_empty() {
            return Err(NodeAmqpError::ReceiverError(
                "Tried to start_receiving without listening to anything".to_string(),
            ));
        }

        // Declare our anonymous queue
        let anon_queue = amqp::QueueBuilder::named("".to_string())
            .exclusive()
            .declare(&mut channel)?;

        debug!("Created queue {}", anon_queue.queue.clone());

        // Actually bind to all the things we've been configured to
        for (exchange, routing_key) in &self.to_bind {
            debug!(
                "Binding routing key {} to {} on exchange {}",
                routing_key, &anon_queue.queue, exchange
            );

            channel.queue_bind(
                anon_queue.queue.clone(),
                exchange.clone(),
                routing_key.clone(),
                false,
                amqp::Table::default(),
            )?;
        }

        let (tx, rx) = channel::unbounded::<IncomingMessage>();
        let _ = thread::spawn(move || {
            let receiver = AmqpReceiverConsumer::new_with_channels(tx);
            let cr = channel
                .basic_consume(
                    receiver,
                    anon_queue.queue,
                    "".to_string(),
                    true,
                    false,
                    true,
                    false,
                    amqp::Table::default(),
                )
                .expect("Failed to call basic_consume");
            debug!("Starting consumer with ctag {}", &cr);
            channel.start_consuming();
            debug!("AmqpReceiver {:?} shutting down", thread::current().id())
        });

        Ok(rx)
    }
}

impl AmqpReceiverConsumer {
    /// Creates a new [AmqpReceiverConsumer] that sends messages it receives to its output channel.
    ///
    /// The other half of that channel is returned by [AmqpReceiver::start_receiving].
    fn new_with_channels(msg_output_channel: channel::Sender<IncomingMessage>) -> Self {
        AmqpReceiverConsumer(msg_output_channel)
    }

    /// Sends an [IncomingMessage] out via the consumer's output channel.
    fn send(&self, msg: IncomingMessage) {
        self.0.send(msg)
    }
}

impl amqp::Consumer for AmqpReceiverConsumer {
    fn handle_delivery(
        &mut self,
        channel: &mut amqp::Channel,
        method: amqp::protocol::basic::Deliver,
        properties: amqp::protocol::basic::BasicProperties,
        body: Vec<u8>,
    ) {
        let msg = IncomingMessage {
            body,
            reply_to: properties.reply_to,
            correlation_id: properties.correlation_id,
            content_type: properties.content_type,
            content_encoding: properties.content_encoding,
            routing_key: method.routing_key,
            exchange: method.exchange,
        };

        self.send(msg);
        channel.basic_ack(method.delivery_tag, false).unwrap();
    }
}
