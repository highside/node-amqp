//! A serializable message and some parameters controlling how it's sent over AMQP.
use amqp::protocol::basic::BasicProperties;

/// What `AmqpSender`s receive. I'm not sure how ergonomic this is in practice, but it's easy to change if it's awkward.
#[derive(Debug, Default, Clone, PartialEq)]
pub struct AmqpEnvelope {
    /// The serialized message.
    pub(crate) msg: Vec<u8>,

    /// The exchange that the message should be sent to.
    pub(crate) exchange: String,

    /// The routing key the message should be sent with.
    pub(crate) routing_key: String,

    /// Extremely complex properties.
    pub(crate) properties: BasicProperties,

    /// Not supported by the amqp crate
    #[allow(unused)]
    mandatory: bool,

    /// Not supported by the amqp crate
    #[allow(unused)]
    immediate: bool,
}

impl AmqpEnvelope {
    /// Create a new `AmqpEnvelope` that, when delivered to an `AmqpSender`, will result in `msg` being sent to `exchange`
    /// with routing key `routing_key`.
    pub fn new<S: Into<String>>(msg: Vec<u8>, exchange: S, routing_key: S) -> Self {
        Self {
            msg,
            exchange: exchange.into(),
            routing_key: routing_key.into(),
            mandatory: false,
            immediate: false,
            properties: BasicProperties::default(),
        }
    }
}
