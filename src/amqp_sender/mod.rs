use amqp::Basic;
use crossbeam::channel;
use std::thread;

pub mod amqp_envelope;
pub mod amqp_wrappable;

use self::amqp_envelope::*;
pub use self::amqp_wrappable::*;
use crate::node_amqp_error::NodeAmqpResult;

/// A worker that receives serializable messages on a [channel] and sends them over AMQP. Instances of [AmqpSender] can
/// be used to send messages from multiple threads.
#[derive(Debug)]
pub struct AmqpSender {
    /// The join handle of the `AmqpSender`'s worker thread. This might not be necessary?
    #[allow(unused)]
    worker_handle: Option<thread::JoinHandle<()>>,

    /// A channel connected to the worker thread's `Receiver`.
    ///
    /// To send messages from multiple threads, clone `outgoing` and send it to those threads. They may then send
    /// arbitrary [AmqpEnvelope]s to it.
    pub outgoing: channel::Sender<AmqpEnvelope>,
}

impl AmqpSender {
    /// Construct a new `AmqpSender`.
    pub fn new(mut channel: amqp::Channel) -> Self {
        // Create the transmitting and receiving sides of the channel that will be used to communicate with the sender
        let (tx, rx) = channel::unbounded();

        // Spawn the worker thread that receives messages from the channel
        let worker = thread::spawn(move || {
            while let Some(req) = rx.recv() {
                AmqpSender::amqp_send(&mut channel, req)
                    .expect("Failed to send message. TODO: Real error handling logic");
            }
        });

        Self {
            worker_handle: Some(worker),
            outgoing: tx,
        }
    }

    /// Wraps `message` in an `AmqpEnvelope`, then enqueues a request to send it to the given exchange and routing key.
    ///
    /// To send messages from multiple threads, use [AmqpSender::outgoing].
    ///
    /// # Examples
    /// ```rust,ignore
    /// #[derive(Serialize)]
    /// struct Example {
    ///   i: i32,
    ///   j: i32,
    /// }
    ///
    /// let Result<(), NodeAmqpError> = sender.send("exchange", "routing.key", Example { i: 1, j: 2 });
    /// ```
    pub fn send<S: Into<String>, M: serde::Serialize>(
        &self,
        exchange: S,
        routing_key: S,
        message: M,
    ) -> NodeAmqpResult<()> {
        let envelope = message.to_message(exchange, routing_key)?;
        self.outgoing.send(envelope);
        Ok(())
    }

    /// Send the contents of `request` over `channel`, setting AMQP parameters appropriately.
    fn amqp_send(channel: &mut amqp::Channel, request: AmqpEnvelope) -> NodeAmqpResult<()> {
        channel.basic_publish(
            request.exchange,
            request.routing_key,
            false,
            false,
            request.properties,
            request.msg,
        )?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {}
