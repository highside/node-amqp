//! [AmqpWrappable] indicates that a type can be sent by an `AmqpSender`.
//!
//! It provides a single method: [AmqpWrappable::to_message]. [AmqpWrappable::to_message] wraps a serialized version of
//! its implementer in an [AmqpEnvelope]. To implement [AmqpEnvelope] without implementing `Serialize`, wait until
//! that's actually possible and use it.
use super::amqp_envelope::*;

/// Provides a `to_message` method that wraps a type in an `AmqpEnvelope`, allowing an `AmqpSender` to send it.
///
/// # TODOs
/// * Don't JSON-serialize everything.
pub trait AmqpWrappable {
    /// Create an `AmqpEnvelope` for sending `self` over AMQP with the given exchange and routing key.
    fn to_message<S: Into<String>>(
        &self,
        exchange: S,
        routing_key: S,
    ) -> Result<AmqpEnvelope, serde_json::Error>;
}

// If you can serialize it, we can put it in an AmqpEnvelope
impl<T> AmqpWrappable for T
where
    T: serde::Serialize,
{
    fn to_message<S: Into<String>>(
        &self,
        exchange: S,
        routing_key: S,
    ) -> Result<AmqpEnvelope, serde_json::Error> {
        // In principle, this works for more than just JSON, but we'll cross that bridge when we come to it
        let own_bytes = serde_json::to_vec(&self)?;
        Ok(AmqpEnvelope::new(own_bytes, exchange, routing_key))
    }
}
