extern crate amqp;

use amqp::AMQPError;
use serde_json::error::Error as SerdeJsonError;

#[derive(Debug)]
pub enum NodeAmqpError {
    AmqpError(AMQPError),
    SerdeError(SerdeJsonError),
    IoError(std::io::Error),
    ReceiverError(String), // TODO more fleshed-out error types
}

impl From<serde_json::error::Error> for NodeAmqpError {
    fn from(e: serde_json::error::Error) -> Self {
        NodeAmqpError::SerdeError(e)
    }
}

impl From<AMQPError> for NodeAmqpError {
    fn from(e: AMQPError) -> Self {
        NodeAmqpError::AmqpError(e)
    }
}

impl From<std::io::Error> for NodeAmqpError {
    fn from(e: std::io::Error) -> Self {
        NodeAmqpError::IoError(e)
    }
}

impl std::fmt::Display for NodeAmqpError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{:?}", &self)
    }
}

impl std::error::Error for NodeAmqpError {
    fn cause(&self) -> Option<&std::error::Error> {
        match self {
            NodeAmqpError::AmqpError(err) => Some(err),
            NodeAmqpError::SerdeError(err) => Some(err),
            NodeAmqpError::IoError(err) => Some(err),
            NodeAmqpError::ReceiverError(_err_string) => None, // TODO improve this
        }
    }
}

pub type NodeAmqpResult<T> = Result<T, NodeAmqpError>;
