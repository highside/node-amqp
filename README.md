[![pipeline status](https://gitlab.com/highside/node-amqp/badges/master/pipeline.svg)](https://gitlab.com/highside/node-amqp/commits/master)

# Overview
`node_amqp` is a crate that wraps highside/rust-amqp-fork>, my fork of
[Andir's fork](https://github.com/andir/rust-amqp) of
[Antti's rust-amqp crate](https://github.com/Antti/rust-amqp).

`node_amqp` is intended for use as a building block for higher-level messaging
 abstractions on top of AMQP.

# [Documentation](https://highside.gitlab.io/node-amqp/node_amqp/)
Documentation is automatically built from [`master`](https://gitlab.com/highside/node-amqp/commits/master).
It'll be available on docs.rs once this crate is polished enough for broader use.