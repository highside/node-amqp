#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate log;
extern crate amqp;
extern crate node_amqp;

use node_amqp::{AmqpReceiver, AmqpSender, AmqpWrappable, NodeAmqpResult};

// We'll send this to ourselves and echo it
#[derive(Debug, Serialize)]
pub struct Example {
    i: i32,
    j: i32,
}

/// Sets up `env_logger` so that it defaults to printing `info`-level log messages and higher.
fn init_logging() {
    // I feel like this is way too elaborate, but whatever. We only need two lines of code to set this up, so I can't
    // complain too much
    let env = env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info");
    env_logger::Builder::from_env(env).init(); // Initialize the logger.
}

pub fn main() -> NodeAmqpResult<()> {
    init_logging();

    // Start by creating a Session (i.e. connection to the broker)
    let mut s = artisanal_utils::get_session()?;

    // Create a sending channel and a receiving channel
    let recv_channel = s.open_channel(1)?;
    let send_channel = s.open_channel(2)?;

    let rx = {
        let mut builder = AmqpReceiver::new();
        builder.listen("amq.topic".to_string(), "routing.key".to_string());
        builder
            .start_receiving(recv_channel)
            .expect("Failed to start receiver")
    };

    // Block until the receiver has probably actually started consuming. Yes, this is gross
    std::thread::sleep(std::time::Duration::from_millis(250));
    let sender = AmqpSender::new(send_channel);
    sender.send("amq.topic", "routing.key", Example { i: 20, j: 35 })?;

    let outgoing = sender.outgoing.clone();
    let _ = std::thread::spawn(move || {
        let another_example = Example { i: 30, j: 40 };
        outgoing.send(
            another_example
                .to_message("amq.topic", "routing.key")
                .expect("Couldn't serialize another_example"),
        );
    });

    // Wait a few ms so that we've probably sent the messages
    std::thread::sleep(std::time::Duration::from_millis(100));

    while let Some(msg) = rx.recv() {
        let msg = format!(
            "{:?}: {:#08X} ({}) bytes. \"{}\"",
            msg,
            msg.body.len(),
            msg.body.len(),
            String::from_utf8_lossy(&msg.body),
        );

        info!("{}", msg);
    }

    for msg in rx {
        info!("{:?}: {:0X} bytes", msg, msg.body.len());
    }

    Ok(())
}

/// A disgusting module full of artisanal hand-crafted utilities used by this example.
mod artisanal_utils {
    use amqp::{AMQPScheme, Options, Session};
    use node_amqp::*;
    use std::default::Default;

    pub fn get_session() -> NodeAmqpResult<Session> {
        Ok(Session::new(Options {
            host: String::from("localhost"),
            login: String::from("guest"),
            password: String::from("guest"),
            scheme: AMQPScheme::AMQP,
            heartbeat: Some(std::time::Duration::from_secs(30)), // I think this is the default

            ..Default::default()
        })?)
    }
}
